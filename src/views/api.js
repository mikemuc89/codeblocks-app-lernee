/* @flow */
import { TextField, UserSchema } from '@omnibly/codeblocks-cmn-schema';
import {
  apiFactory,
  type ApiFactoryReturnType,
  type ResourceDataType
} from '@omnibly/codeblocks-cmn-schema/src/api';
import { LinkMapper, MenuItemMapper, NotificationMapper } from '@omnibly/codeblocks-mod-basis/src/schema';

const notifyResponseHandler = (response: ResourceDataType) => response;

export const NOTIFY_URL = '/api/notify';

const notify = apiFactory(NOTIFY_URL, notifyResponseHandler);

const initResponseHandler = ({ data, fields }: ResourceDataType) => ({
  data: {
    links: LinkMapper(data.links),
    menu: MenuItemMapper(data.menu),
    notifications: NotificationMapper(data.notifications),
    user: UserSchema(data.user)
  },
  fields: {
    search: TextField(fields.search)
  }
});

export const URL = '/api/init';

export default (apiFactory(URL, initResponseHandler, undefined, undefined, {
  notify
}): ApiFactoryReturnType & {
  notify: ApiFactoryReturnType
});
