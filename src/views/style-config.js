/* @flow */
module.exports = {
  images: {
    logo: {
      original: {
        disabled: require('../assets/images/logos/omnibly-disabled.svg').default,
        hover: require('../assets/images/logos/omnibly-hover.svg').default,
        normal: require('../assets/images/logos/omnibly-normal.svg').default
      },
      white: {
        disabled: require('../assets/images/logos/omnibly-white-disabled.svg').default,
        hover: require('../assets/images/logos/omnibly-white-hover.svg').default,
        normal: require('../assets/images/logos/omnibly-white-normal.svg').default
      }
    }
  }
};
