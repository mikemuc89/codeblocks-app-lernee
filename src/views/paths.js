/* @flow */
export default (Object.freeze({
  ...require('./paths.imports.json').default,
  ...require('@omnibly/codeblocks-mod-admin').default,
  ...require('@omnibly/codeblocks-mod-articles').default,
  ...require('@omnibly/codeblocks-mod-auth').default,
  ...require('@omnibly/codeblocks-mod-basis').default,
  ...require('@omnibly/codeblocks-mod-comments').default,
  ...require('@omnibly/codeblocks-mod-courses').default,
  ...require('@omnibly/codeblocks-mod-exams').default,
  ...require('@omnibly/codeblocks-mod-news').default,
  ...require('@omnibly/codeblocks-mod-polls').default
}): Object);
