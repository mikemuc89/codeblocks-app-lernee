/* @flow */
import {
  apiFactory,
  type ApiFactoryReturnType,
  type ResourceDataType
} from '@omnibly/codeblocks-cmn-schema/src/api';
import { ArticleMapper, ArticleSchema } from '@omnibly/codeblocks-mod-articles/src/schema';
import { CourseMapper } from '@omnibly/codeblocks-mod-courses/src/schema';
import { NewsMapper } from '@omnibly/codeblocks-mod-news/src/schema';

export const URL = '/api/home';

const responseHandler = ({ data = {} }: ResourceDataType) => ({
  data: {
    courses: CourseMapper(data.courses),
    featured: ArticleSchema(data.featured),
    news: NewsMapper(data.news),
    primaryArticles: ArticleMapper(data.primary_articles),
    secondaryArticles: ArticleMapper(data.secondary_articles)
  }
});

export default (apiFactory(URL, responseHandler): ApiFactoryReturnType);
