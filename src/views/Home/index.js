/* @flow */
import * as React from 'react';
import { Abstract, Advert, Button, Card, Column, Row, Sign, View } from '@omnibly/codeblocks-cmn-components';
import { AsLeafView, type LeafViewPropsType, type LeafViewType } from '@omnibly/codeblocks-cmn-components/src/hocs';
import api from './api';

const Home = ({ form: { data }, loading, navigate }: LeafViewType) => (
  <View.Centered loading={loading}>
    <Card>
      <Card.Content>
        {data.featured &&
          (({ color, ref_details, summary, ...props }: Object) => (
            <Abstract.Featured backgroundColor={color} href="#/ARTICLES/ARTICLE/SHOW" hrefData={ref_details} {...props}>
              {summary}
            </Abstract.Featured>
          ))(data.featured)}
        {data.primaryArticles && (
          <Column.Set>
            {data.primaryArticles.map(({ color, ref_details, summary, ...props }: Object, idx: number) => (
              <Column key={idx} colCount={3}>
                <Abstract backgroundColor={color} href="#/ARTICLES/ARTICLE" hrefData={ref_details} {...props}>
                  {summary}
                </Abstract>
              </Column>
            ))}
          </Column.Set>
        )}
      </Card.Content>
    </Card>
    <Card.PostScriptum>
      <Row>
        <Button href="#/ARTICLES/ARTICLE/LIST" kind={Button.KINDS.SUBMIT} justify>
          Więcej artykułów
        </Button>
      </Row>
    </Card.PostScriptum>
    <Column.Set outer>
      <Column colSpan="2">
        <Card>
          <Card.Header>Kursy</Card.Header>
          <Card.Content>
            {data.courses &&
              data.courses.map(({ color, ref_details, ...item }: Object, idx: number) => (
                <Sign.Avatar
                  key={idx}
                  backgroundColor={color}
                  href="/COURSES/COURSE/SHOW"
                  hrefData={ref_details}
                  {...item}
                />
              ))}
          </Card.Content>
        </Card>
        <Card.PostScriptum>
          <Row>
            <Button href="#/COURSES/LIST" kind={Button.KINDS.SUBMIT} justify>
              Więcej kursów
            </Button>
          </Row>
        </Card.PostScriptum>
      </Column>
      <Column>
        <Advert />
      </Column>
    </Column.Set>
    <Column.Set outer>
      <Column>
        <Card>
          <Card.Header>Aktualności</Card.Header>
          <Card.Content>
            {data.news &&
              data.news.map(({ ref_details, summary, ...props }: Object, idx: number) => (
                <Abstract key={idx} href="#/NEWS/NEWS/SHOW" hrefData={ref_details} {...props}>
                  {summary}
                </Abstract>
              ))}
          </Card.Content>
        </Card>
        <Card.PostScriptum>
          <Row>
            <Button href="#/NEWS/NEWS/LIST" kind={Button.KINDS.SUBMIT} justify>
              Więcej aktualności
            </Button>
          </Row>
        </Card.PostScriptum>
      </Column>
      <Column colSpan="2">
        <Card>
          <Card.Content>
            {((articles: React.Node) => articles && <Column.Set>{articles}</Column.Set>)(
              data.secondaryArticles &&
                data.secondaryArticles.map(({ color, ref_details, summary, ...props }: Object, idx: number) => (
                  <Column key={idx}>
                    <Abstract backgroundColor={color} href="/ARTICLES/ARTICLE/SHOW" hrefData={ref_details} {...props}>
                      {summary}
                    </Abstract>
                  </Column>
                ))
            )}
          </Card.Content>
        </Card>
        <Card.PostScriptum>
          <Row>
            <Button href="#/ARTICLES/ARTICLE/LIST" kind={Button.KINDS.SUBMIT} justify>
              Więcej artykułów
            </Button>
          </Row>
        </Card.PostScriptum>
      </Column>
    </Column.Set>
  </View.Centered>
);

export default (AsLeafView({ api })(Home): React.ComponentType<LeafViewPropsType>);
