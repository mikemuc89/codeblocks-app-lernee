/* @flow */
import * as React from 'react';
import { Footer, Icon, Input, Menu, Notification, TopMenu } from '@omnibly/codeblocks-cmn-components';
import MediaContext from '@omnibly/codeblocks-cmn-components/src/contexts/MediaContext';
import {
  AsRootView,
  WithRedirect,
  type EventParamsType,
  type RedirectCallbackParamsType,
  type RootViewPropsType,
  type RootViewType
} from '@omnibly/codeblocks-cmn-components/src/hocs';
import useMedia from '@omnibly/codeblocks-cmn-components/src/hooks/useMedia';
import { hasRodoAgreement } from '@omnibly/codeblocks-cmn-utils';
import * as CONSTANTS from '../constants';
import errorConfig from '../errors';
import EVENTS from '../events';
import api from './api';
import navigation from './paths';
import i18n from '../literals.i18n';
import { images } from './style-config';
import styles from './main.scss';

const App = WithRedirect({
  onRedirect: ({ navigate }: RedirectCallbackParamsType) => {
    if(!hasRodoAgreement()) {
      navigate('/RODO');
    }
  },
  timeout: 3000
})(({ Content, fieldHandler, fetch, form: { data }, navigate }: RootViewType) => {
  const { breakpoint } = useMedia();
  const mediaContextValue = React.useMemo(() => ({ breakpoint }), [breakpoint]);

  const actions = {
    notify: (meta: Object) => fetch(api.notify)({ meta }),
    search: (query: string) => navigate('/SEARCH', { query })
  };

  return (
    <div className={styles.App}>
      <MediaContext.Provider value={mediaContextValue}>
        <TopMenu>
          <TopMenu.Name href="#/HOME">{CONSTANTS.NAME}</TopMenu.Name>
          {data.links &&
            data.links.map(({ text, ...props }: Object, idx: number) => (
              <TopMenu.Link key={idx} {...props} right>
                {text}
              </TopMenu.Link>
            ))}
          <Input.Search {...fieldHandler('search')} onAction={actions.search} onEnter={actions.search} />
        </TopMenu>
        <Menu>
          <Menu.Logo src={images.logo.original.normal} href="#/HOME" />
          {data.menu &&
            data.menu.map(({ text, href, ...props }: Object, idx: number) => (
              <Menu.Link key={idx} href={href} {...props}>
                {text}
              </Menu.Link>
            ))}
        </Menu>
        <div className={styles.Notifications}>
          <Notification.Cookies />
          {data.notifications &&
            data.notifications.map(({ text, ...props }: Object, idx: number) => (
              <Notification key={idx} onNotify={actions.notify} {...props}>
                {text}
              </Notification>
            ))}
        </div>
        <div className={styles.Content}>{Content}</div>
        <Footer>
          <Icon id="copyright" />
          <span>{CONSTANTS.FOOTER}</span>
        </Footer>
      </MediaContext.Provider>
    </div>
  );
});

const events = {
  [EVENTS.LOGIN]: ({ data, fetch }: EventParamsType) => fetch(api)({ meta: { data } }),
  [EVENTS.LOGOUT]: ({ data, fetch }: EventParamsType) => fetch(api)({ meta: { data } })
};

export default (AsRootView({ api, errorConfig, events, i18n, navigation })(App): React.ComponentType<RootViewPropsType>);
