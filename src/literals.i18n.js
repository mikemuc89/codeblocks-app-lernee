/* @flow */
export default (Object.freeze({
  ...require('./literals.i18n.json').default,
  ...require('@omnibly/codeblocks-mod-admin/src/literals.i18n.json').default,
  ...require('@omnibly/codeblocks-mod-articles/src/literals.i18n.json').default,
  ...require('@omnibly/codeblocks-mod-auth/src/literals.i18n.json').default,
  ...require('@omnibly/codeblocks-mod-basis/src/literals.i18n.json').default,
  ...require('@omnibly/codeblocks-mod-comments/src/literals.i18n.json').default,
  ...require('@omnibly/codeblocks-mod-courses/src/literals.i18n.json').default,
  ...require('@omnibly/codeblocks-mod-exams/src/literals.i18n.json').default,
  ...require('@omnibly/codeblocks-mod-news/src/literals.i18n.json').default,
  ...require('@omnibly/codeblocks-mod-polls/src/literals.i18n.json').default
}): Object);
