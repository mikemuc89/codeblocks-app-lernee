/* @flow */
export { triggerEvent } from '@omnibly/codeblocks-cmn-components/src/hocs/WithEventsListener';

export default (Object.freeze({
  ...require('@omnibly/codeblocks-mod-admin/src/events').default,
  ...require('@omnibly/codeblocks-mod-articles/src/events').default,
  ...require('@omnibly/codeblocks-mod-auth/src/events').default,
  ...require('@omnibly/codeblocks-mod-basis/src/events').default,
  ...require('@omnibly/codeblocks-mod-comments/src/events').default,
  ...require('@omnibly/codeblocks-mod-courses/src/events').default,
  ...require('@omnibly/codeblocks-mod-exams/src/events').default,
  ...require('@omnibly/codeblocks-mod-news/src/events').default,
  ...require('@omnibly/codeblocks-mod-polls/src/events').default
}): Object);
