/* @flow */
export default ({
  '/api/home': require('./home').default,
  '/api/init': require('./init').default,
  '/api/notify': require('./notify').default
}: Object);
