/* @flow */
import * as React from 'react';
import ReactDOM from 'react-dom';
import LayerRootContainer from '@omnibly/codeblocks-cmn-components/src/containers/LayerRootContainer';
import InitialDataContext from '@omnibly/codeblocks-cmn-components/src/contexts/InitialDataContext';
import polyfills from '@omnibly/codeblocks-cmn-env/src/app/polyfills';
import { unitize } from '@omnibly/codeblocks-cmn-utils';
import App from './views';

const INITIAL_APPDATA_KEY = '__APPDATA__';

const getInitialData = (viewId: string) => window[INITIAL_APPDATA_KEY] && window[INITIAL_APPDATA_KEY][viewId];

document.addEventListener('DOMContentLoaded', () => {
  polyfills();
  const root = document.getElementById('root');

  if (root) {
    ReactDOM.hydrate(
      <InitialDataContext.Provider value={getInitialData}>
        <LayerRootContainer>
          <App />
        </LayerRootContainer>
      </InitialDataContext.Provider>,
      root
    );
  }
});
