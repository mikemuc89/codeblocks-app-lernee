/* @flow */
export default (Object.freeze({
  ...require('./mocks.dev').default,
  ...require('@omnibly/codeblocks-mod-admin/src/mocks.dev').default,
  ...require('@omnibly/codeblocks-mod-articles/src/mocks.dev').default,
  ...require('@omnibly/codeblocks-mod-auth/src/mocks.dev').default,
  ...require('@omnibly/codeblocks-mod-basis/src/mocks.dev').default,
  ...require('@omnibly/codeblocks-mod-comments/src/mocks.dev').default,
  ...require('@omnibly/codeblocks-mod-courses/src/mocks.dev').default,
  ...require('@omnibly/codeblocks-mod-exams/src/mocks.dev').default,
  ...require('@omnibly/codeblocks-mod-news/src/mocks.dev').default,
  ...require('@omnibly/codeblocks-mod-polls/src/mocks.dev').default
}): Object);
