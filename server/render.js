/* @flow */
import config from '../config';
import App from '../src/views/index';
import paths from '../src/paths';

const clientName = 'lernee';
const clientStaticDir = '/static/';

const initApi = require('../src/views/api');

const serverFactory = require('@omnibly/codeblocks-cmn-server');
const app = serverFactory({ config, initApi, App, paths, clientName, clientStaticDir });

const clientHost = config.get('CLIENT_HOST');
const clientPort = config.get('CLIENT_PORT');
const clientProtocol = config.get('CLIENT_PROTOCOL');

const client = `${clientHost}:${clientPort}`;

app.listen(clientPort, clientHost, () => {
  console.log(`Server is listening at ${clientPort}`);
});
