const webpackConfig = require('@omnibly/codeblocks-cmn-env');

const config = require('../config');
const NAME = 'server';

module.exports = webpackConfig({
  config,
  output: NAME.toLowerCase(),
  kind: 'server'
});
