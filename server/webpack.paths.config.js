const webpackConfig = require('@omnibly/codeblocks-cmn-env');

const config = require('../config');
const NAME = 'paths';

module.exports = webpackConfig({
  config,
  output: NAME.toLowerCase(),
  kind: 'paths'
});
