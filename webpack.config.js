/* eslint-disable flowtype/require-valid-file-annotation */
const webpackConfig = require('@omnibly/codeblocks-cmn-env');
const config = require('./config');

const NAME = 'lernee';

module.exports = webpackConfig({
  basePath: require.resolve('.'),
  config,
  html: {
    language: 'pl-PL',
    title: 'Omnibly - kurs programowania - Python, JavaScript, HTML, CSS'
  },
  kind: 'client',
  output: NAME.toLowerCase()
});
